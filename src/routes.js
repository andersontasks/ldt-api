const { Router } = require('express');
const TarefaController = require('./controllers/TarefaController');

const routes = Router();

routes.get('/tarefas', TarefaController.index); // Listar todas as tarefas
routes.get('/tarefa/:id', TarefaController.show); // Listar uma tarefa
routes.put('/tarefa/:id', TarefaController.update); // Editar tarefa
routes.delete('/tarefa/:id', TarefaController.destroy); // Remover tarefa

module.exports = routes;
