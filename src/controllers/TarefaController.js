const { Tarefa } = require('../models');

module.exports = {
    async index(request, response) {
       const tarefas = await Tarefa.findAll();
       return response.json(tarefas);
    },

    async show(request, response) {
        const tarefa = await Tarefa.findByPk(request.params.id);        
        return response.json(tarefa);
    },

    async update(request, response) {
        const { titulo, descricao, responsavel, data_entrega, status } = request.body.tarefa;        

        tarefa = await Tarefa.update({
            titulo: titulo,
            descricao: descricao,
            responsavel: responsavel,
            data_entrega: data_entrega,
            status: status,
          }, {
            where: {
              id: request.params.id
            }
          }).then(result => {
              if(result) 
                  return "Tarefa atualizada com sucesso!";
              else 
                  return "Erro ao atualizar tarefa, por favor, tente novamente!";
          });
          
        return response.json(tarefa);        
    },

    async destroy(request, response) {                
        tarefa = await Tarefa.destroy({
            where: {
                id: request.params.id
            }
        }).then(result => {
            if(result) 
                  return "Tarefa removida com sucesso!";
              else 
                  return "Erro ao remover tarefa, por favor, tente novamente!";
        });

        return response.json(tarefa);
    }
}