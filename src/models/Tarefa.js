module.exports = (sequelize, DataTypes) => {
    const Tarefa = sequelize.define('Tarefa', {
      titulo: DataTypes.STRING,
      descricao: DataTypes.STRING,
      responsavel: DataTypes.STRING,
      data_entrega: DataTypes.DATE,
      status: DataTypes.STRING,
    });
  
    return Tarefa;
}