'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Tarefas', [{
      titulo: 'Desenvolver app mobile',
      descricao: 'Desenvolver aplicativo com finalidade de gerenciar atividades feitas no dia dia',
      responsavel: 'Ronaldo',
      data_entrega: '2020-04-01',
      status: 'Pendente',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      titulo: 'Desenvolver sistema web',
      descricao: 'Desenvolver sistema web com finalidade de medir os dados financeiros da empresa',
      responsavel: 'Neymar',
      data_entrega: '2020-06-01',
      status: 'Pendente',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      titulo: 'Migrar banco de dados',
      descricao: 'Efetuar a migração dos bancos x,y,z que estão atualmente no MySQL para SQLServer',
      responsavel: 'Robinho',
      data_entrega: '2020-03-01',
      status: 'Pendente',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      titulo: 'Implementar rotinas de backup no servidor',
      descricao: 'Buscar por software robustos para implementar rotinas de backup',
      responsavel: 'Arrascaeta',
      data_entrega: '2020-04-01',
      status: 'Pendente',
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Tarefas', null, {});
  }
};
